*- Avem nevoie de mai mult timp !*  fraza îi pulsa în minte distrugând orice alt gând înainte de a putea prinde contur.

 Liniștea din jur nu avea nici un sens, contrastul violent făcând-o să-și piardă momentan orientarea.
 
 Pașii îi curgeau absenți pe trotuarul gri, iar strada părea un fundal în două dimensiuni străpuns pe alocuri de câteva siluete stinghere. Libera... ? Stai! Suntem in secvența libera?

 -Hei! Te duci undeva? o siluetă se desprinse din fundal, tăindu-i calea și întrerupându-i șirul gândurilor.

 Un grup de persoane vag familiare stăteau de vorbă, blocând parțial trecerea.

-Salut Abic. Nu, ma gândeam... răspunse ea, încercând să absoarbă dintr-o privire realitatea înconjurătoare.

 În fața ei stătea acum o puștoaică cam de aceeași vârstă, gulerul jachetei albe tăindu-i parcă radacina gâtului măsliniu.

-Știi Abic, nu te prinde deloc culoarea asta...

 Fata o privi o clipă nedumerită, încercând să facă legatura între această observație și discuția pe care tocmai o părăsise. Negăsind nici una, ingoră remarca și răspunse pe un ton ușor iritat:

-Tocmai ai avut un nou episod...?

 *Avem nevoie de mai mult timp...* acum era parcă un refren care ți se lipeste de creier ca o gumă mestecată de talpă, deloc violent, dar agasant:

-Nu. Da... Hai sa nu vorbim despre asta acum... E toată lumea aici? Sunteți gata?

-Da. Intrăm. spuse Abic arătând cu capul spre ușa culisantă din sitclă transparentă, pe care defilau animații din divese jocuri ale momentului. 

 Era într-adevăr libera... Dintre toate secvențele din ciclu, libera era singura când puteau alege activitățile, fără justificare. Desigur, putei oricand abuza de faza de încărcare din orice secvență, dar într-o societate ca aceasta, în care secvențele erau planificate riguros, o pierdere de randament datorată unei faze de încărcare utilizată necorespunzător te putea costa puncte de performanță, iar asta nu oricine își putea permite...

 Oricum... de ce ai fi facut-o ? Ciclurile sunt într-un continuu proces de optimizare, iar Organizatorii fac tot posibilul ca acestea să fie perfect adaptate persoanelor active.
 Numărul de secvențe din ciclu putea varia lejer, dar libera apărea de regulă odată la 4-5 secvențe. Chiar și secvențele puteau avea durate diferite, dar niciodată in cadrul aceluiași ciclu.
 Organizatorii spuneau, în timpul fazelor de învățare, că, de la Prima Activare, secvențele au o continuă tendință de reducere, ceea ce a dus la un numar mai mare de secvențe pe ciclu.

 -Ikai? Incepem!
  Își examină sumar avatarul, era modelul ei preferat, mascul, inălțime medie, parul lung, fără cască. Combinezonul albasru cu motive abstracte din linii frânte alb cu roșu era o țintă sigură, dar ea era un jucător abil, iar stridența culorilor făcea să-i crească orgoliul odată cu palmaresul...


"Rjjjjjjjj...!"


 pana acum...


Ceee !?!? Nici nu apucase să intre în joc și să se orienteze cât de de cât și era deja reperată de o santinelă?

CINE ? privi rapid în jur. Cunoștea nivelul, era unul rapid. Santinelele n-aveau o poziție foarte înălțată față de jucători.

 Principiul de joc era simplu, fiecare jucător înmagazina o cantitate fixă de energie pe care o putea distribui în obiecte, deasupra cărora se putea transfera pentru a câștiga în altitudine. Putea absorbi energia oricărui obiect aflat la aceeași înălțime sau mai jos. Odată ajuns deasupra santinelelor, le putea absorbi energia. Jocul se termina când toate santinelele erau eliminate.

 Ușor de zis... Depistase santinela care-i consuma încet dar sigur energia. Alese un spațiu indepărtat, ferit de linia directă de observație a santinelelor și se transferă acolo, fara nici un obiect suplimentar, trebuia sa scape cat mai repede.

 *Noroc că santinelele nu se pot transfera*, își zise oarecum cu ușurare când se văzu la adăpost.

 Îi plăcea în mod deosebit acest joc, îi părea combinația perfectă dintre acțiune și planificare. Era ca un circuit de parcour, dar fără presiunea momentului și cu un plus discret de strategie și orientare spațială. Puteai să îți dozezi timpul...

 -Cine-i ăla? întrebă făcând un gest cu capul în direcția santinelei aflată acum cu spatele la ele. Avea un combinezon compus din hexagoane în degrade, parcă într-o continuă metamorfoză, cu tente de la gri șters până la vișiniu intens.

 -Elaa...? răspunse Abic în cască
 Sigur l-ai mai văzut, e de câteva cicluri in sectorul nostru. Am avut chiar câteva faze de invățare impreună.

*Hmm... chiar așa...?* își zise Ikai
-Poate-ar trebui să-l privesc de mai sus, sa-l cunosc mai bine... răspunse cu voce tare, abordând un zâmbet malițios.

 Schimbul de replici nu durase mai mult de un minut dar o adusese deja pe o poziție egala cu cea a lui Elaa, trebuia doar să găsească poziția finala de unde să-l poată absorbi fără, bineînțles, să se expună celorlalte santinele.
 *Asta e, gotcha!* exclamă ea în gând, făcând ultimul salt. Se orientă spre poziția unde ar fi trebuit să se afle Elaa. *Stai! Ce?* Dezorientare. Panică. *Nu se poate! A știut?* 

 De fapt, când un jucator dobândea o poziție superioară santinelelor, se transforma el însuși într-o santinelă, ceea ce-i permitea să absoarbă o santinelă, dar în același timp, cea mai apropiată santinelă devenea jucător și se putea transfera.

 "Kjjjjjjjjjj" negru...
